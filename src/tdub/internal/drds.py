"""Internal module for DS vs DS stuff."""

from __future__ import annotations

from pathlib import PosixPath

import matplotlib.pyplot as plt
import numpy as np
import tdub.config
from pygram11 import fix1d

from tdub.art import one_sided_comparison_plot, separation_plot, setup_tdub_style
from tdub.data import quick_files
from tdub.frames import raw_dataframe


def bdt_cut_plots(
    source: PosixPath,
    branch: str = "bdtres03",
    lumi: float = 140,
    lo_1j1b: float = 0.55,
    hi_1j1b: float = 0.76,
    lo_2j1b: float = 0.40,
    hi_2j1b: float = 0.70,
    lo_2j2b: float = 0.45,
    hi_2j2b: float = 0.775,
    bins_1j1b: tuple[int, float, float] = (35,0.13,0.865),
    bins_2j1b: tuple[int, float, float] = (30,0.10,0.85),
    bins_2j2b: tuple[int, float, float] = (27,0.125,0.85625),
    bins_removed_1j1b: tuple[int, float, float] = (10,0.55,0.76),
    bins_removed_2j1b: tuple[int, float, float] = (12,0.4,0.7),
    bins_removed_2j2b: tuple[int, float, float] = (12,0.45,0.775),
    thesis: bool = False,
) -> None:
    """Geneate plots showing BDT cuts."""
    setup_tdub_style()
    source = PosixPath(source)
    qf = quick_files(source)

    def drds_histograms(
        dr_df,
        ds_df,
        region,
        branch="bdtres03",
        weight_branch="weight_nominal",
        nbins=12,
        xmin=0.2,
        xmax=0.9,
        flows=True,
    ):
        dr_hist, err = fix1d(
            dr_df[branch].to_numpy(),
            bins=nbins,
            range=(xmin, xmax),
            weights=dr_df[weight_branch].to_numpy() * lumi,
            flow=flows,
        )
        ds_hist, err = fix1d(
            ds_df[branch].to_numpy(),
            bins=nbins,
            range=(xmin, xmax),
            weights=ds_df[weight_branch].to_numpy() * lumi,
            flow=flows,
        )
        return dr_hist, ds_hist

    def ttPS_histograms(
        ttbar_df,
        ttbar_AFII_df,
        ttbar_AFII_PS_df,
        region,
        branch="bdtres03",
        weight_branch="weight_nominal",
        nbins=12,
        xmin=0.2,
        xmax=0.9,
    ):
        ttbar_hist, err = fix1d(
            ttbar_df[branch].to_numpy(),
            bins=nbins,
            range=(xmin, xmax),
            weights=ttbar_df[weight_branch].to_numpy() * lumi,
            flow=True,
        )
        ttbar_AFII_hist, err = fix1d(
            ttbar_AFII_df[branch].to_numpy(),
            bins=nbins,
            range=(xmin, xmax),
            weights=ttbar_AFII_df[weight_branch].to_numpy() * lumi,
            flow=True,
        )
        ttbar_AFII_PS_hist, err = fix1d(
            ttbar_AFII_PS_df[branch].to_numpy(),
            bins=nbins,
            range=(xmin, xmax),
            weights=ttbar_AFII_PS_df[weight_branch].to_numpy() * lumi,
            flow=True,
        )
        ttbar_PS_hist = ttbar_hist + (ttbar_AFII_PS_hist-ttbar_AFII_hist)
        ttbar_PS_hist *= (np.sum(ttbar_hist) / np.sum(ttbar_PS_hist)) # normalised distribution
        return ttbar_hist, ttbar_PS_hist

    branches = [branch, "weight_nominal", "reg1j1b", "reg2j1b", "reg2j2b", "OS"]
    dr_df = raw_dataframe(qf["tW_DR"], branches=branches)
    ds_df = raw_dataframe(qf["tW_DS"], branches=branches)
    ttbar_df = raw_dataframe(qf["ttbar"], branches=branches)
    ttbar_AFII_PS_df = raw_dataframe(qf["ttbar_PS"], branches=branches)
    ttbar_AFII_df = raw_dataframe(qf["ttbar_AFII"], branches=branches)

    ##################

    tW_1j1b, ttbar_1j1b = drds_histograms(
        dr_df.query(tdub.config.SELECTION_1j1b),
        ttbar_df.query(tdub.config.SELECTION_1j1b),
        "1j1b",
        branch,
        nbins=bins_removed_1j1b[0],
        xmin=bins_removed_1j1b[1],
        xmax=bins_removed_1j1b[2],
        flows=False,
    )
    tW_1j1b /= np.sum(tW_1j1b)
    ttbar_1j1b /= np.sum(ttbar_1j1b)
    fig, ax = separation_plot(
        tW_1j1b,
        ttbar_1j1b,
        r'$tW$',
        r'$t\bar{t}$',
        np.linspace(bins_removed_1j1b[1], bins_removed_1j1b[2], bins_removed_1j1b[0] + 1),
        thesis=thesis,
        extra_lines=['1j1b'],
    )
    fig.savefig("overlay_1j1b.pdf")
    plt.close(fig)
    ##################

    tW_2j1b, ttbar_2j1b = drds_histograms(
        dr_df.query(tdub.config.SELECTION_2j1b),
        ttbar_df.query(tdub.config.SELECTION_2j1b),
        "2j1b",
        branch,
        nbins=bins_removed_2j1b[0],
        xmin=bins_removed_2j1b[1],
        xmax=bins_removed_2j1b[2],
        flows=False,
    )
    tW_2j1b /= np.sum(tW_2j1b)
    ttbar_2j1b /= np.sum(ttbar_2j1b)
    fig, ax = separation_plot(
        tW_2j1b,
        ttbar_2j1b,
        r'$tW$',
        r'$t\bar{t}$',
        np.linspace(bins_removed_2j1b[1], bins_removed_2j1b[2], bins_removed_2j1b[0] + 1),
        thesis=thesis,
        extra_lines=['2j1b'],
    )
    fig.savefig("overlay_2j1b.pdf")
    plt.close(fig)
    ##################

    tW_2j2b, ttbar_2j2b = drds_histograms(
        dr_df.query(tdub.config.SELECTION_2j2b),
        ttbar_df.query(tdub.config.SELECTION_2j2b),
        "2j2b",
        branch,
        nbins=bins_removed_2j2b[0],
        xmin=bins_removed_2j2b[1],
        xmax=bins_removed_2j2b[2],
        flows=False,
    )
    tW_2j2b /= np.sum(tW_2j2b)
    ttbar_2j2b /= np.sum(ttbar_2j2b)
    fig, ax = separation_plot(
        tW_2j2b,
        ttbar_2j2b,
        r'$tW$',
        r'$t\bar{t}$',
        np.linspace(bins_removed_2j2b[1], bins_removed_2j2b[2], bins_removed_2j2b[0] + 1),
        thesis=thesis,
        extra_lines=['2j2b'],
    )
    fig.savefig("overlay_2j2b.pdf")
    plt.close(fig)
    ##################

    ttbar, ttbar_PS = ttPS_histograms(
        ttbar_df.query(tdub.config.SELECTION_1j1b),
        ttbar_AFII_PS_df.query(tdub.config.SELECTION_1j1b),
        ttbar_AFII_df.query(tdub.config.SELECTION_1j1b),
        "1j1b",
        branch,
        nbins=bins_1j1b[0],
        xmin=bins_1j1b[1],
        xmax=bins_1j1b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        ttbar,
        ttbar_PS,
        np.linspace(bins_1j1b[1], bins_1j1b[2], bins_1j1b[0] + 1),
        thesis=thesis,
        extra_lines=[r'1j1b $t\bar{t}$ PS'],
    )
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_1j1b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_1j1b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_1j1b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_1j1b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_1j1b, 1.0, color="gray", alpha=0.55)
    fig.savefig("ttPS_1j1b.pdf")
    plt.close(fig)

    ##################

    ttbar, ttbar_PS = ttPS_histograms(
        ttbar_df.query(tdub.config.SELECTION_2j1b),
        ttbar_AFII_PS_df.query(tdub.config.SELECTION_2j1b),
        ttbar_AFII_df.query(tdub.config.SELECTION_2j1b),
        "2j1b",
        branch,
        nbins=bins_2j1b[0],
        xmin=bins_2j1b[1],
        xmax=bins_2j1b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        ttbar,
        ttbar_PS,
        np.linspace(bins_2j1b[1], bins_2j1b[2], bins_2j1b[0] + 1),
        thesis=thesis,
        extra_lines=[r'2j1b $t\bar{t}$ PS'],
    )
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_2j1b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_2j1b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_2j1b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_2j1b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_2j1b, 1.0, color="gray", alpha=0.55)
    fig.savefig("ttPS_2j1b.pdf")
    plt.close(fig)

    ##################

    ttbar, ttbar_PS = ttPS_histograms(
        ttbar_df.query(tdub.config.SELECTION_2j2b),
        ttbar_AFII_PS_df.query(tdub.config.SELECTION_2j2b),
        ttbar_AFII_df.query(tdub.config.SELECTION_2j2b),
        "2j2b",
        branch,
        nbins=bins_2j2b[0],
        xmin=bins_2j2b[1],
        xmax=bins_2j2b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        ttbar,
        ttbar_PS,
        np.linspace(bins_2j2b[1], bins_2j2b[2], bins_2j2b[0] + 1),
        thesis=thesis,
        extra_lines=[r'2j2b $t\bar{t}$ PS'],
    )
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_2j2b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_2j2b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_2j2b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_2j2b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_2j2b, 1.0, color="gray", alpha=0.55)
    fig.savefig("ttPS_2j2b.pdf")
    plt.close(fig)

    ##################

    dr, ds = drds_histograms(
        dr_df.query(tdub.config.SELECTION_1j1b),
        ds_df.query(tdub.config.SELECTION_1j1b),
        "1j1b",
        branch,
        nbins=bins_1j1b[0],
        xmin=bins_1j1b[1],
        xmax=bins_1j1b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        dr,
        ds,
        np.linspace(bins_1j1b[1], bins_1j1b[2], bins_1j1b[0] + 1),
        thesis=thesis,
        extra_lines=['1j1b DR vs DS'],
    )
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_1j1b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_1j1b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_1j1b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_1j1b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_1j1b, 1.0, color="gray", alpha=0.55)
    fig.savefig("drds_1j1b.pdf")
    plt.close(fig)

    ##################

    dr, ds = drds_histograms(
        dr_df.query(tdub.config.SELECTION_2j1b),
        ds_df.query(tdub.config.SELECTION_2j1b),
        "2j1b",
        branch,
        nbins=bins_2j1b[0],
        xmin=bins_2j1b[1],
        xmax=bins_2j1b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        dr,
        ds,
        np.linspace(bins_2j1b[1], bins_2j1b[2], bins_2j1b[0] + 1),
        thesis=thesis,
        extra_lines=['2j1b DR vs DS'],
    )
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_2j1b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_2j1b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_2j1b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_2j1b, 1.0, color="gray", alpha=0.55)
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_2j1b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    fig.savefig("drds_2j1b.pdf")
    plt.close(fig)

    ##################

    dr, ds = drds_histograms(
        dr_df.query(tdub.config.SELECTION_2j2b),
        ds_df.query(tdub.config.SELECTION_2j2b),
        "2j2b",
        branch,
        nbins=bins_2j2b[0],
        xmin=bins_2j2b[1],
        xmax=bins_2j2b[2],
    )
    fig, ax, axr = one_sided_comparison_plot(
        dr,
        ds,
        np.linspace(bins_2j2b[1], bins_2j2b[2], bins_2j2b[0] + 1),
        thesis=thesis,
        extra_lines=['2j2b DR vs DS'],
    )
    ax.fill_betweenx([-1, 1.0e5], -1.0, lo_2j2b, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], -1.0, lo_2j2b, color="gray", alpha=0.55)
    ax.fill_betweenx([-1, 1.0e5], hi_2j2b, 1.0, color="gray", alpha=0.55)
    axr.fill_betweenx([-200, 200], hi_2j2b, 1.0, color="gray", alpha=0.55)
    ymid = ax.get_ylim()[1] * 0.67
    xmid = (lo_2j2b - ax.get_xlim()[0]) * 0.5 + ax.get_xlim()[0]
    ax.text(xmid, ymid, "Excluded", ha="center", va="center", color="dimgray", size=15)
    fig.savefig("drds_2j2b.pdf")
    plt.close(fig)

